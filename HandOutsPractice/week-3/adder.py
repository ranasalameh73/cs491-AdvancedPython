#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 20:39:37 2018

@author: umesh
"""

''' Module adder.py '''

def add_item(item, item_list = None):    
    if item_list == None:
        item_list = []
    item_list.append(item)
    print(item_list)