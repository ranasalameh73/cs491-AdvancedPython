#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 21:13:03 2018

@author: umesh
"""

''' Module connect2 '''

def connect(uname, *args, **kwargs):
    print(uname)
    for arg in args:
        print(arg)
    for key in kwargs.keys():
        print(key, ":", kwargs[key])

connect('admin', 'ilovecats', server='localhost', port=9160)
