#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 18:00:12 2018

@author: umesh
"""

class A:
    def __init__(self, value):
        self.value = value
    def __eq__(self, other):
        if isinstance(other, B):
            print('Comparing an A with a B')
            return other.value == self.value
        print('Couldnot compare A with other')
        return NotImplemented
    
class B:
    def __init__(self, value):
        self.value = value
    def __eq__(self, other):
        print('Couldnot compare B with other')