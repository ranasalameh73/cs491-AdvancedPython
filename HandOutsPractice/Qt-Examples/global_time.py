#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 21:57:34 2018

@author: umesh
"""

from PyQt5.QtCore import QDateTime, Qt

now = QDateTime.currentDateTime()
print("Local datetime: ", now.toString(Qt.ISODate))
print("Universal Datetime: ", now.toUTC().toString(Qt.ISODate))
print("The offset from UTC is: {0} seconds".format(now.offsetFromUtc()))