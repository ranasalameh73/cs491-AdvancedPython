#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 18:12:42 2018

@author: umesh
"""

from randomizer import SixteenDices
from input_processor import InputProcessor

words_to_match = list()
def match_dictionary():
        with open('words.txt', 'r') as fp:
            for line in fp:
                words_to_match = fp.read().splitlines()
        words_to_match = [x.upper() for x in 
                          [y for y in words_to_match if len(y) >= 3]]

def isword(string):
    if string in words_to_match:
        return True
    return False
    
def find_words(input_list, input_visited_track, i, j, string):
    input_visited_track[i][j] = True
    string = string+input_list[i][j]
    print(str(string))
    if isword(string.upper()):
        print(string.upper())
    
    for row in range(i-1, i+2):
        if row >= len(input_list):
            break
        for col in range(j-1, j+2):
            if col >= len(input_list[0]):
                break
            if row>=0 and col>=0:
                if input_visited_track[row][col] == False:
                    find_words(input_list, input_visited_track, 
                               row, col, string)
    string = string[:-1]
    input_visited_track[i][j] = False
    return

def find_word(input_word, input_grid):
    for row in range(0, len(input_grid)):
        for col in range(0, len(input_grid)):
            if find_word_helper(input_word, row, col, input_grid):
                return True
    return False

def find_word_helper(word, row, col, input_grid):
    if word == '':
        return True
    elif row<0 or row>=4 or col<0 or col>=4 or word[:1] != input_grid[row][col]:
        return False
    else:
        safe = input_grid[row][col]
        input_grid[row][col] = '*'
        rest = word[1:len(word)]
        result = find_word_helper(rest, row-1, col-1, input_grid) \
            or find_word_helper(rest, row-1, col, input_grid)   \
            or find_word_helper(rest, row-1, col+1, input_grid) \
            or find_word_helper(rest, row, col-1, input_grid)   \
            or find_word_helper(rest, row, col+1, input_grid)   \
            or find_word_helper(rest, row+1, col-1, input_grid) \
            or find_word_helper(rest, row+1, col, input_grid)   \
            or find_word_helper(rest, row+1, col+1, input_grid) \
            
        input_grid[row][col] = safe
        return result
    
    
class Main:
    grids = 4
    def initialize_display(self):
        self.sixteendices = SixteenDices()
        self.sixteendices.display_dices()
        print('Start typing your words!' \
              '(press enter after each word' \
              ' and enter "X" when done)' )  

    def process_input(self):
        self.inputprocessor = InputProcessor()
        while True:
            input_word = input()
            if str(input_word).upper() == 'X':
                break
            else:
                self.inputprocessor.add_to_input(input_word)

    def word_combination(self):
        points = 0
        in_grids = self.sixteendices.get_dice_grid()
        input_words = self.inputprocessor.get_input_list()
        for j in range(0, len(input_words)):
            if find_word(input_words[j], in_grids):
                    if len(input_words[j])<3:
                        print('The word ', input_words[j], 'is too short.')
                        pass
                    if len(input_words[j]) == 3 or len(input_words[j]) == 4:
                        print('The word ', input_words[j], 'is worth 1 point')
                        points += 1
                    if len(input_words[j]) == 5:
                        print('The word ', input_words[j], 'is worth 2 point')
                        points += 2
                    if len(input_words[j]) == 6:
                        print('The word ', input_words[j], 'is worth 3 point')
                        points += 3
                    if len(input_words[j]) == 7:
                        print('The word ', input_words[j], 'is worth 5 point')
                        points += 5
                    if len(input_words[j]) >= 8:
                        print('The word ', input_words[j], 'is worth 11 point')
                        points += 11
    
            else:
                print('The word ', input_words[j], 
                      'is not present in the grid.')
        self.score = points
        print('Your total score is ', points, 'points')
                
    
    def setSixteenDices(self, sixteen_dices):
        self.sixteendices = sixteen_dices
    
    def setInputProcessor(self, input_processor):
        self.inputprocessor = input_processor
    
    def getScore(self):
        return self.score



