#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 23:56:02 2018

@author: umesh
"""

# Mandatory Imports
import csv

def smallest_difference(file_name):
    """
        Reads a csv file, Tries to find a person with smallest difference 
        between two quizzes, five and six in this case
        
        param   : file_name is the csvfile to be read
        returns : the name of the person with smallest difference between
                  the two quizzes(5, 6 in our case)
    """
    
    try:
        csv_file = open(file_name, 'r')
    except FileNotFoundError as fne:
        print("FileNotFoundError:", fne.args[1])
    except:
        print("UnknownError: Something's wrong")
    else:
        csv_contents = csv.reader(csv_file, delimiter = ",")
        next(csv_contents)  # Skip the first row for good ;)
        diff_min = dict([[x[0], abs(int(x[5])-int(x[6]))] 
                         for x in csv_contents]) #  Make a dictionary
    
        person = min(diff_min, key = diff_min.get)
        return person 
        csv_file.close()  # Gracefully closing the file

if __name__ == "__main__":
    print(smallest_difference('class.csv'))
