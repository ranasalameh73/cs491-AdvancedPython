#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  1 15:00:34 2018

@author: umesh
"""

import socketserver
from blackjack import BlackjackGame 

class MyTCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        pass
        
    
class MyTCPServer(socketserver.TCPServer):
    def serve_blackjack(self, c):
        BlackjackGame(c).play()

if __name__ == "__main__":
    HOST, PORT = "localhost", 9198
    server = MyTCPServer((HOST, PORT), MyTCPHandler)   
    while True:
        c, a = server.get_request()
        print(c)
        server.serve_blackjack(c)
        