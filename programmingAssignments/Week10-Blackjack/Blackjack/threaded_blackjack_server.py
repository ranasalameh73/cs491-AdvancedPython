import socket
from blackjack import BlackjackGame
import threading

def serve_game(c):
        BlackjackGame(c).play()

if __name__ == "__main__":
    # Create IPv4 TCP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Make socket re-usable before natural release
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Bind to localhost:9000
    s.bind(("",9010))
    s.listen(5)
    while True:
        c,a = s.accept()
        print('accepted connection from {}'.format(c) )
		# Handle client connection in a new thread. 
        t = threading.Thread(target=serve_game, args=(c,))
        t.start()
